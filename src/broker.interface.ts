export interface IFeaturedBrokersBgColors {
    [key: string]: string;
}

export interface IIBDestination {
    name: string;
    url: string;
}

export interface IIntroducingBrokerLinks {
    referalUrlBase: string;
    destinations: IIBDestination[];
}

/**
 * Config of socket connection
 */
export interface INewsConfiguration {
    url: string;
    port: string;
    path: string;
    title?: string;
    icon: string;
    isVip?: boolean;
}

export interface ILinksMenuItems {
    title: string;
    links: IListItem[];
}

export interface IListItem {
    label: string;
    help?: string;
    url?: string;
    onClick?: () => void;
    brokerUrl?: string;
    skipTranslation?: boolean;
    target?: '_blank' | '_self';
}

export type PredefinedLinkLabel =
    'Terms of Business' |
    'Risk Disclosure' |
    'Order Execution Policy' |
    'Privacy Policy' |
    'Conflict of Interest Policy' |
    'Client categ. notice' |
    'ICF Policy';

export interface IKycAgreementLink {
    text: PredefinedLinkLabel;
    link: string;
}

export interface IHideFeaturesVersion30 {
    symbolTiles: boolean;
    resize: boolean;
    reordering: boolean;
    symbolInfo: boolean;
    analyzeProfitability: boolean;
    detachASP: boolean;
    detachWatchlist: boolean;
    priceRange: boolean;
    newsTabASP: boolean;
    newsBlockASP: boolean;
}

export interface IBrokerConfiguration {
    /** @deprecated */ mobileAppName?: string;
    /** @deprecated */ dialogsTitle?: string;
    /** @deprecated */ name?: string;
    /** @deprecated */ lcgMyLcgText?: string;
    /** @deprecated */ productName?: string;

    canonicalUrl?: string;
    binaryUrl?: string;

    isLcg?: boolean;
    isKawase?: boolean;
    isFxProMarkets?: boolean;
    myLcgTitle?: string; // text for views on LCG and LCG white labels only (e.g. "My LCG", "My Accendo" etc.)
    productAppName?: string; // text for dialog headers, menus and other views ("Spotware cTrader", "LCG Trader" etc.)
    legalName?: string; // text for Quick Trade settings agreement (e.g. "cTrader Ltd."), defaults to `${brokerTitle}`
    cTraderIdName?: string; // text for views (e.g. "LCG ID", "FxPro Markets ID" etc.), defaults to `${brokerTitle} ID`
    immutableSimplifiedName: string; // name of the broker that should be used in models and which we share with cServer, .NET etc

    newsPort: number;
    newsUrl: string;

    crowdinEnable: boolean;

    defaultCommodityVolume: number;
    defaultVolume: number;
    brokerTitle: string;
    mobileAppDescription?: string;
    proxyListUrl: string;
    packageId: string;
    tel: string;
    fax: string;
    web: string;
    email: string;
    favicon: string;
    splashImgUrl: string;
    aboutDialogImgUrl: string;
    dialogImgUrl: string;
    dialogImgTopUrl: string;
    authDialogImgTopUrl: string;
    dialogImgBottomUrl: string;
    dialogImgPixelBackgroundUrl: string;
    notificationImgUrl: string;
    ctWebUrl: string;
    defaultCurrency: string;
    accountCurrencies: string[];
    defaultLeverage: number;
    accountLeverages: number[];
    brokerId: string;
    chartShootBrokerId: string;
    proxies: string[];
    cHubProxies: string[];
    mobileGoogleAnalyticsId: string;
    googleAnalyticsId: string;
    basicGoogleAnalyticsId?: string;
    appsFlyerId?: string;
    appsFlyerRedirectionLink?: string;
    news: INewsConfiguration[];
    helpUri: string;
    helpLabel: string;
    labelsColor: string;
    leftActiveTab: string;
    leftBottomColor: string;
    facebookAppId: number;
    authServiceUrl: string;
    profileManagementToolTip: string;
    aboutDialogClass?: string;
    notificationManagerShowConfigureLink?: boolean;

    // broker provides these:
    safeChargeIsEnabled: boolean;
    safeChargeCode: string;
    safeChargeMerchantId: string;
    safeChargeMerchantSiteId: string;
    safeChargeMinimumDeposit: number;
    safeChargeMaximumDeposit: number;
    safeChargeDefaultDeposit: number;
    ignorePlist: boolean;
    reorderNotFilledMenuItemsOnMobile: boolean;

    safeChargeDepositBaseUrl: string;
    safeChargeDepositErrorBaseUrl: string;
    safeChargeDepositResponseBaseUrl: string;

    postMessageRedirectUrl: string;
    lcgUrl?: string;
    lcgVisitSite?: string;
    lcgMobileUseFrames: any;
    lcgUrlDepositToken?: any;
    lcgUrlMyLcgToken?: any;
    lcgUrlLiveAccountToken?: any;
    lcgUrlLiveAccount?: any;
    lcgUrlProfileToken?: any;
    lcgUrlAnalysis?: any;
    lcgCopyRightTDDIenabled?: boolean;
    lcgUrlDemoAccount?: string;
    lcgUrlDepositFrameToken?: string;

    profileManagementExplanation?: string;

    multipleWatchlistsCountLimit: number;

    fixApiAvailable?: boolean;
    fixApiPricePort?: number;
    fixApiTradePort?: number;
    fixApiTargetCompID?: string;

    fixApiHelpLink?: string;

    filterWhiteLabelAccounts?: boolean;

    lcgDemoAccountNumber?: number;
    lcgDemoAccountPassword?: string;

    isDinPayEnabled?: boolean;
    dinPayBackOfficeUrl?: string;
    calgoUrl?: string;
    ctNetUrl?: string;
    ctIosUrl?: string;
    ctAndroidUrl?: string;
    cmirrorUrl?: string;
    marketWatchUrl?: string;
    swapFreeAllowed?: boolean;
    skipReconnect?: boolean;

    hubProxyListUrl?: string;
    lcgUrlLiveChat?: string;
    liveChatUrl?: string;
    isChatEnabled?: boolean; // Kawase. Chat for Profile Management

    cServerWebsocketBinaryProtocols?: string;
    cServerWebsocketJSONProtocols?: string;
    cHubWebsocketBinaryProtocols?: string;
    cHubWebsocketJSONProtocols?: string;
    lcgWhitelabelBrokerId?: string;
    forceUsingMobileApp?: boolean;
    launchAppUrl?: string;
    marketAppNameAndroid?: string;
    marketAppNameIos?: string;
    isQuickStartGuideEnabled?: string;
    showWhatsNewOnFirstAppStart?: boolean;
    isPaySafeEnabled?: boolean;
    feedbackLink?: string;
    icpLicense?: string;
    dealsMaxRows?: number;
    linksMenuItems?: ILinksMenuItems[];
    isIBEnabled?: boolean;
    featuredBrokers?: string[];
    featuredBrokersBgColors?: IFeaturedBrokersBgColors;
    idFrameWidth?: number;
    idFrameHeight?: number;
    proxyPort?: number;
    namePrefix?: string;
    binaryOptionEnabled?: boolean;
    userActivityPingURL?: string;
    introducingBrokersLinks?: IIntroducingBrokerLinks;
    showSignInPage?: boolean;
    binaryDeepLinkIOSCheckUrl?: string;
    binaryDeepLinkAndroidCheckUrl?: string;
    binaryDeepLink?: string;
    binaryDeepLinkPlayMarketUrl?: string;
    binaryDeepLinkAppStoreUrl?: string;
    appEnviroment?: string;
    termsAndConditionsUrl?: string;
    _proxyListSnapshot?: {
        date: Date;
        proxies: any [];
    };
    youtubeFeaturesPlaylistId?: string;
    youtubeLearnPlaylistId?: string;
    metaInfo?: {
        title?: string;
        description?: string;
        keywords?: string;
    };
    chartShotUrl?: string;
    tradingCentralPartnerId?: string;
    tradingCentralPartnerToken?: string;
    ctidFrameHeight?: number;
    stopLimitEnabled?: boolean;
    whiteLabelId?: number;
    showCtidInsideIosCordova?: boolean;
    isWhiteLabel?: boolean;
    logAnalyticEvents?: boolean;
    disableAccountPropertiesChange?: boolean;

    dialogHeaderImgUrl_light: string;
    dialogHeaderImgUrl_dark: string;
    placeOrderButtonEnabled?: boolean;

    becomeSignalProviderNotAllowed?: boolean;

    allowCreatingAccountsFor?: string[];
    plantId?: string;

    isBrandedMirror?: boolean;
    debugMode?: boolean;
    cHubMirrorJSONProtocol?: string;
    cHubMirrorBinaryProtocol?: string;
    canonicalUrlMirror?: string;
    defaultLocale?: string;
    faviconMirror?: string;
    introVideoDisabled?: boolean;
    myLcgTitleAnalysis?: string;
    IBtermsLink?: string;

    MW_login?: number;
    MW_passwordHash?: string;
    MW_productId?: string;
    MW_brokerLogoUrl?: string;
    MW_brokerUrl?: string;
    MW_downloadUrl?: string;
    MW_downloadTitle?: string;
    MW_cTwebUrl?: string;
    disableDepositButton?: boolean;
    isolationBrokerName?: boolean;
    isolationPlantId?: boolean;
    brokerName?: string;

    kycRiskScoringEnabled?: boolean;
    kycAgreementLinks?: IKycAgreementLink[];
    CIDGUISettingsEnvironmentName?: string;

    amazonMarketDataEnabled?: boolean;

    isCopyEnabled?: boolean;

    hideFeaturesVersion30?: IHideFeaturesVersion30;

    disableTradeApp?: boolean;

    showLastTrades?: number;
}
