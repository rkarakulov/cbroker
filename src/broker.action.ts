import typescriptFsa from 'typescript-fsa';
import {IBrokerConfiguration} from './broker.interface';

export namespace BrokerAction {
    const actionCreator = typescriptFsa('Broker');

    export const loadDone = actionCreator<IBrokerConfiguration>('SET_DONE');
    export const loadFailed = actionCreator('SET_FAILED');
}
