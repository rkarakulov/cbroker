import {BrokerAction} from '../broker.action';
import {IBrokerConfiguration} from '../broker.interface';
import {brokerReducer} from '../broker.reducer';
import {IBrokerStore} from '../broker.store.interface';
import {NetworkStatus} from 'ctrader-helpers/src/network/network.constant';

describe('broker.reducer', () => {
    test('should set configuration', () => {
        const initialState = {
            data: {},
        } as IBrokerStore;

        const actual = brokerReducer(initialState, BrokerAction.loadDone({
            plantId: 'asd',
        } as IBrokerConfiguration));

        const expected = {
            data: {
                plantId: 'asd',
            },
            networkStatus: NetworkStatus.Done,
        } as IBrokerStore;

        expect(actual)
            .toEqual(expected);
    });

    test('should change network status to failed', () => {
        const initialState = {
            data: {},
        } as IBrokerStore;

        const actual = brokerReducer(initialState, BrokerAction.loadFailed());

        const expected = {
            data: {},
            networkStatus: NetworkStatus.Failed,
        } as IBrokerStore;

        expect(actual)
            .toEqual(expected);
    });
});