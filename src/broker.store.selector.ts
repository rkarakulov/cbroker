import {IBrokerStoreSegment, IBrokerStore} from './broker.store.interface';
import {IBrokerConfiguration} from './broker.interface';

export const brokerStoreSelector = (store: IBrokerStoreSegment): IBrokerStore => store.broker;

export const brokerDataSelector = (store: IBrokerStoreSegment): IBrokerConfiguration => store.broker.data;
