/** defined in webpack */
import {getBrandingVersion} from '../_helpers/parse.helper';
import {getAppVersionString, getAppVersionStringShort} from '../_helpers/version.helper';

declare global {
    export const BUNDLE_VERSION: string;
}

export const codeVersionSelector = (): string => getAppVersionString(BUNDLE_VERSION, getBrandingVersion());

export const shortCodeVersionSelector = (): string => getAppVersionStringShort(BUNDLE_VERSION, getBrandingVersion());
