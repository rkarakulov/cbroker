import {createSelector} from 'reselect';
import {IBrokerStoreSegment} from '../broker.store.interface';
import {brokerDataSelector} from '../broker.store.selector';
import {createPathSelector} from 'ctrader-helpers';

export const productNameSelector = createSelector<IBrokerStoreSegment,
    string, string,
    string>(
    [
        createPathSelector(brokerDataSelector, 'productAppName'),
        createPathSelector(brokerDataSelector, 'brokerTitle'),
    ],
    (productAppName, brokerTitle) => {
        return productAppName
            ? productAppName
            : brokerTitle;
    },
);
