import {createSelector} from 'reselect';
import {IBrokerStoreSegment} from '../broker.store.interface';
import {brokerDataSelector} from '../broker.store.selector';
import {createPathSelector} from 'ctrader-helpers';

export const plantIdSelector = createSelector<IBrokerStoreSegment, string, string>(
    [
        createPathSelector(brokerDataSelector, 'brokerId'),
    ],
    brokerId => {
        return brokerId.split('_')[0].toLowerCase();
    },
);
