import {createSelector} from 'reselect';
import {IBrokerStoreSegment} from '../broker.store.interface';
import {brokerDataSelector} from '../broker.store.selector';
import {createPathSelector} from 'ctrader-helpers';

export const isCurrentPlantIdSelectorFactory = () => createSelector<IBrokerStoreSegment,
    { plantId: string },
    string,
    string,
    boolean>(
    createPathSelector(brokerDataSelector, 'plantId'),
    (state, props) => props.plantId,
    (brokerPlantId, plantId) => {
        return brokerPlantId === plantId;
    },
);
