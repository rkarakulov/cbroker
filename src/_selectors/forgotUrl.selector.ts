import {createSelector} from 'reselect';
import {IBrokerStoreSegment} from '../broker.store.interface';
import {brokerDataSelector} from '../broker.store.selector';
import {createPathSelector, withEndSlash} from 'ctrader-helpers';

export const forgotUrlSelector = createSelector<IBrokerStoreSegment, string, string>(
    [
        createPathSelector(brokerDataSelector, 'authServiceUrl'),
    ],
    authServiceUrl => {
        return `${withEndSlash(authServiceUrl)}reset`;
    },
);
