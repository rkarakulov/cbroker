import {createSelector} from 'reselect';
import {IBrokerStoreSegment} from '../broker.store.interface';
import {brokerDataSelector} from '../broker.store.selector';
import {createPathSelector} from 'ctrader-helpers';

export const whiteLabelIdSelector = createSelector<IBrokerStoreSegment, number, number>(
    [
        createPathSelector(brokerDataSelector, 'whiteLabelId'),
    ],
    whiteLabelId => {
        return whiteLabelId || 0;
    },
);
