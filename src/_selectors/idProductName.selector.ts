import {createSelector} from 'reselect';
import {IBrokerStoreSegment} from '../broker.store.interface';
import {brokerDataSelector} from '../broker.store.selector';
import {createPathSelector} from 'ctrader-helpers';

export const idProductNameSelector = createSelector<IBrokerStoreSegment,
    string,
    string>(
    createPathSelector(brokerDataSelector, 'cTraderIdName'),
    result => result || 'cTrader ID',
);
