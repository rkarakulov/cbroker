import {createSelector} from 'reselect';
import {IBrokerStoreSegment} from '../broker.store.interface';
import {productNameSelector} from './productName.selector';

export const clientAgentSelector = createSelector<IBrokerStoreSegment, string, string>(
    productNameSelector,
    productName => `${productName} Web`,
);
