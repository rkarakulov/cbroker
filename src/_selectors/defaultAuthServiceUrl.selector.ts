import {createSelector} from 'reselect';
import {IBrokerStoreSegment} from '../broker.store.interface';
import {getDefaultAuthServiceUrl} from '../_helpers/url.helper';
import {IBrokerConfiguration} from '../broker.interface';
import {brokerDataSelector} from '../broker.store.selector';

export const defaultAuthServiceUrlSelector = createSelector<IBrokerStoreSegment,
    IBrokerConfiguration,
    string>(
    brokerDataSelector,
    broker => {
        return getDefaultAuthServiceUrl(broker);
    },
);
