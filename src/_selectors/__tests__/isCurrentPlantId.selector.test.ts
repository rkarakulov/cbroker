import {IBrokerStore} from '../../broker.store.interface';
import {isCurrentPlantIdSelectorFactory} from '../isCurrentPlantId.selector';
import {IBrokerConfiguration} from '../../broker.interface';

describe('createIsCurrentPlantIdSelector', () => {
    test('should return true when plantId of store equal received plantId', () => {
        const state = {
            broker: {
                data: {
                    plantId: 'test',
                } as IBrokerConfiguration,
            } as IBrokerStore,
        };

        const selectIsCurrentPlantId = isCurrentPlantIdSelectorFactory();
        const actual = selectIsCurrentPlantId(state, {plantId: 'test'});

        expect(actual)
            .toBeTruthy();
    });

    test('should return false when plantId of store not equal received plantId', () => {
        const state = {
            broker: {
                data: {
                    plantId: 'test',
                } as IBrokerConfiguration,
            } as IBrokerStore,
        };

        const selectIsCurrentPlantId = isCurrentPlantIdSelectorFactory();
        const actual = selectIsCurrentPlantId(state, {plantId: 'test1'});

        expect(actual)
            .toBeFalsy();
    });

    test('should cached results for same parameters', () => {
        const state = {
            broker: {
                data: {
                    plantId: 'test',
                } as IBrokerConfiguration,
            } as IBrokerStore,
        };

        const selectIsCurrentPlantId = isCurrentPlantIdSelectorFactory();
        selectIsCurrentPlantId(state, {plantId: 'test'});
        selectIsCurrentPlantId(state, {plantId: 'test'});

        expect(selectIsCurrentPlantId.recomputations())
            .toBe(1);
    });

    test('should cached results for same parameters even if the state changes', () => {
        const state = {
            broker: {
                data: {
                    plantId: 'test',
                } as IBrokerConfiguration,
            } as IBrokerStore,
        };

        const selectIsCurrentPlantId = isCurrentPlantIdSelectorFactory();
        selectIsCurrentPlantId(state, {plantId: 'test'});
        selectIsCurrentPlantId({
            ...state,
            broker: {
                ...state.broker,
            },
        }, {
            plantId: 'test',
        });

        expect(selectIsCurrentPlantId.recomputations())
            .toBe(1);
    });
});
