import {forgotUrlSelector} from '../forgotUrl.selector';

describe('forgotUrlSelector', () => {
    test('should select forgot url from auth service url', () => {
        expect(forgotUrlSelector.resultFunc('testUrl'))
            .toBe('testUrl/reset');
    });
});
