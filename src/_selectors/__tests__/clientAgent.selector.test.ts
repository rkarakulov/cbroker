import {IBrokerStore} from '../../broker.store.interface';
import {clientAgentSelector} from '../clientAgent.selector';
import {IBrokerConfiguration} from '../../broker.interface';

describe('selectClientAgent', () => {
    test('should return productAppName with Web postfix', () => {
        const productAppName = 'productAppName';

        const state = {
            broker: {
                data: {
                    productAppName,
                } as IBrokerConfiguration,
            } as IBrokerStore,
        };

        expect(clientAgentSelector(state))
            .toBe(`${productAppName} Web`);
    });
});
