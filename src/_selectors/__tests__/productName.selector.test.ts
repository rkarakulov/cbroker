import {IBrokerStore} from '../../broker.store.interface';
import {productNameSelector} from '../productName.selector';
import {IBrokerConfiguration} from '../../broker.interface';

describe('selectBrokerProductName', () => {
    test('should return productAppName if defined', () => {
        const productAppName = 'productAppName';

        const state = {
            broker: {
                data: {
                    productAppName,
                } as IBrokerConfiguration,
            } as IBrokerStore,
        };

        expect(productNameSelector(state))
            .toBe(productAppName);
    });
});
