import {parse} from 'url';
import {basename} from 'path';

export function getBrandingName(): string {
    const scripts = document.getElementsByTagName('script');
    const script = scripts[scripts.length - 1];
    const url = parse(script.src);
    const brand = document.documentElement.getAttribute('data-broker')
        || (url.host || '').split('.')[0];

    return brand || 'UNKNOWN_BROKER';
}

export function getBrandingVersion(): string {
    return document.documentElement.getAttribute('data-broker-version') || '';
}

export function getBrandingLink(path: string): string {
    path = path.replace(/common\/branding\/[^/]*\//, '');

    return `/branding/${getBrandingName()}/${path}?version=${getBrandingVersion()}`;
}

export function getBrandingImageUrl(path: string): string {
    if (!path) {
        return '';
    }

    const filename = basename(path);
    return getBrandingLink(`img/${filename}`);
}
