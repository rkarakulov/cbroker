import {IBrokerConfiguration} from '../broker.interface';
import {getBrandingName, getBrandingLink} from './parse.helper';
import {getReduxService} from 'ctrader-helpers';
import {BrokerAction} from '../broker.action';

export function getBrandingPromise(): Promise<IBrokerConfiguration> {
    return new Promise<IBrokerConfiguration>((resolve, reject) => {
        initBranding(resolve, reject);
    });
}

function initBranding(successCallback: (broker: IBrokerConfiguration) => void, failCallback: () => void) {
    if (process.env.NODE_ENV !== 'production') {
        const brandingName = getBrandingName();

        const config = require(`branding/${brandingName}/config.json`);

        successCallback(config);
        return;
    }

    loadBrokerConfig(successCallback, failCallback);
    return;
}

function loadBrokerConfig(callback: (broker: IBrokerConfiguration) => void, failCallback: () => void) {
    const url = getBrandingLink('config.json');
    const xhr = new XMLHttpRequest();

    xhr.open('GET', url);

    xhr.onload = () => {
        if (xhr.status === 200) {
            const data = xhr.responseText;
            try {
                const brokerConfig = decodeBroker(data);
                callback(brokerConfig);
            } catch (err) {
                onError(url, err);
                failCallback();
            }
        }
        else {
            onError(url);
            failCallback();
        }
    };

    xhr.onerror = () => {
        onError(url);
        failCallback();
    };

    xhr.send();
}

function decodeBroker(config: string): IBrokerConfiguration {
    const f = String.fromCharCode;
    let s = '';
    const t = JSON.parse;
    let c;
    for (let i = 0; i < config.length; i += 1) {
        c = config.charCodeAt(i);
        s += f(c - 1);
    }
    try {
        return t(s);
    } catch (error1) {
        try {
            return t(config);
        } catch (error2) {
            console.error('Error parsing config:\n\n\n', s, config);
            return {} as IBrokerConfiguration;
        }
    }
}

function onError(url: string, errorThrown?: Error) {
    getReduxService()
        .dispatch(BrokerAction.loadFailed);
}
