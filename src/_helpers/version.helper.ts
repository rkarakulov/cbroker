export function getAppVersionString(codeVersion: string, brokerVersion: string): string {
    let appVersionTag;

    if (brokerVersion) {
        appVersionTag = `${codeVersion}-${brokerVersion}`;
    }
    else {
        console.info('No commit hash, using current timestamp as versionTag.'); // tslint:disable-line:no-console
        appVersionTag = codeVersion || String(Date.now());
    }

    return appVersionTag;
}

export function getAppVersionStringShort(codeVersion: string, brokerVersion: string): string {
    return getAppVersionString(codeVersion, brokerVersion)
        .substr(0, 7);
}
