import {IBrokerConfiguration} from '../broker.interface';

export function getDefaultAuthServiceUrl(broker: IBrokerConfiguration): string {
    const brokerAuthServiceUrl = broker.authServiceUrl;
    const immutableSimplifiedName = broker
        .immutableSimplifiedName
        .replace(/\s/g, '')
        .toLowerCase();
    const brokerDefaultAuthServiceUrl = `https://${immutableSimplifiedName}.id.ctrader.com/`;

    return brokerAuthServiceUrl || brokerDefaultAuthServiceUrl;
}

export function getSignUpUrl(broker: IBrokerConfiguration): string {
    const authServiceUrl = getDefaultAuthServiceUrl(broker);

    return authServiceUrl + 'signup';
}

export function getSilentSignUpUrl(broker: IBrokerConfiguration): string {
    return getSignUpUrl(broker) + '?onSuccess=cTwebSilent';
}
