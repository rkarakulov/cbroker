import {getAppVersionString, getAppVersionStringShort} from '../version.helper';

describe('version.helper', () => {
    test('getAppVersionString should combine code and broker version', () => {
        expect(getAppVersionString('codeVersion', 'brokerVersion'))
            .toEqual('codeVersion-brokerVersion');
    });

    test('getAppVersionStringShort should return shorted string of appVersion', () => {
        expect(getAppVersionStringShort('codeVersion', 'brokerVersion'))
            .toEqual('codeVer');
    });
});
