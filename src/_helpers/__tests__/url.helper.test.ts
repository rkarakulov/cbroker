import {getDefaultAuthServiceUrl} from '../url.helper';
import {IBrokerConfiguration} from '../../broker.interface';

describe('url.helper', () => {
    describe('getDefaultAuthServiceUrl', () => {
        test('should authServiceUrl field from broker configuration' +
            ' if authServiceUrl field is defined in broker configuration', () => {
            const authServiceUrl = 'authServiceUrl';
            const immutableSimplifiedName = 'immutableSimplifiedName';
            const broker = {
                immutableSimplifiedName,
                authServiceUrl,
            } as IBrokerConfiguration;

            const actual = getDefaultAuthServiceUrl(broker);

            const expected = authServiceUrl;

            expect(actual)
                .toEqual(expected);
        });

        test('should default authServiceUrl' +
            ' if authServiceUrl field is NOT defined in broker configuration', () => {
            const immutableSimplifiedName = 'immutableSimplifiedName';
            const broker = {
                immutableSimplifiedName,
            } as IBrokerConfiguration;

            const actual = getDefaultAuthServiceUrl(broker);

            const expected = `https://${immutableSimplifiedName.toLowerCase()}.id.ctrader.com/`;

            expect(actual)
                .toEqual(expected);
        });
    });
});
