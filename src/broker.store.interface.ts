import {IBrokerConfiguration} from './broker.interface';
import {NetworkStatus} from 'ctrader-helpers';

export type IBrokerStore = {
    data: IBrokerConfiguration;
    networkStatus: NetworkStatus;
};

export const brokerInitialState = {
    data: {},
    networkStatus: NetworkStatus.None,
};

export type IBrokerStoreSegment = {
    broker: IBrokerStore;
};
