import {brokerInitialState} from './broker.store.interface';
import {reducerWithInitialState} from 'typescript-fsa-reducers';
import {BrokerAction} from './broker.action';
import {NetworkStatus} from 'ctrader-helpers';

export const brokerReducer = reducerWithInitialState(brokerInitialState)
    .case(BrokerAction.loadDone, (state, configuration) => {
        return {
            ...state,
            data: configuration,
            networkStatus: NetworkStatus.Done,
        };
    })
    .case(BrokerAction.loadFailed, state => {
        return {
            ...state,
            networkStatus: NetworkStatus.Failed,
        };
    });
